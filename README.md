# Rake Project

This repository contains Agaric's set of Rake tools for Drupal 7/8 development and deployment.

These tools have certain expectations for the two project types they can handle (Drupal 7, and Drupal 8). To be documented...(e.g. config/vendor/web locations, theme compilation, etc)

# Using this repository

It is expected that this repository is included into your project by making it a sub-module. The submodule should live inside the rakelib folder at the root of the project.

To configure your project, subsequently copy the Rakelib.example file to the root of your project as Rakefile and configure as needed.

View the available actions in the rake file with: `rake -T`.

Your project must include the ability to run ruby/rake, of course.

For ddev, use the following in `.ddev/config`:
```
webimage_extra_packages: [rake]
```
## Configuration

The Rakefile.example contains an example structure.

These are the top-level configuration variables:
```
ENVIRONMENTS - the hash of target deployment environments, and settings for reaching/using them

```

When performing an rsync for a deploy, the file exclude_deploy will be checked for. If this file exists, it will be used as the list of exclusions. See `man rsync` for more info. When using exclude_deploy, be sure to include `sites/default/files` in the list!
