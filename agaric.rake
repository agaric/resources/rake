require 'date'
require 'rake/clean'
require 'shellwords'

CLEAN.include("#{BUILDDIR}")

ENVIRONMENTS.keys.each do |env|
  settings_source = "#{BUILDDIR}/#{env}/#{DRUPAL}/sites/default/#{env}.settings.php"
  settings_target = "#{BUILDDIR}/#{env}/#{DRUPAL}/sites/default/#{SETTINGS_TARGET}"

  release_host = ENVIRONMENTS[env]["host"]
  release_path = ENVIRONMENTS[env]["path"]
  release_backups = ENVIRONMENTS[env]["backups"]
  release_tag = ENVIRONMENTS[env]["tag"]
  drush_path = ENVIRONMENTS[env]["drush"]
  drush_path = drush_path ? drush_path : "drush"
  release_tag = release_tag ? release_tag : 'HEAD'

  # compass_env = "compass_#{env}".to_sym
  build_env = "build_#{env}".to_sym
  upload_env = "upload_#{env}".to_sym
  deploy_env = "deploy_#{env}".to_sym

  file "#{BUILDDIR}/#{env}" => :clean do
    mkdir_p "#{BUILDDIR}/#{env}"
    sh "git archive #{release_tag} #{DRUPAL} | tar -x -C #{BUILDDIR}/#{env}"
    if defined? CONFIG_REPO then
      # TODO: determine commit handling
      sh "git clone --depth=1 #{CONFIG_REPO} #{BUILDDIR}/#{env}/config"
    end
  end

  # task compass_env do
  #   Dir.glob("build/#{env}/#{DRUPAL}/sites/*/themes/**/config.rb") do |project|
  #     sh "compass compile -e production --force #{File.dirname(project)}"
  #   end
  # end

  file settings_target => settings_source do
    cp settings_source, settings_target
  end

  build_version = "#{BUILDDIR}/#{env}/#{DRUPAL}/BUILD_VERSION.txt"

  file build_version do
    sh "git describe --always --long #{release_tag} > #{build_version}"
  end

  desc "Build the #{env} environment."
  # TODO: add sass compile
  task build_env => [:clean, "#{BUILDDIR}/#{env}", settings_target, build_version]

  # Tack on the composer run when running a Drupal 8 build:
  if TYPE == "Drupal8" then
    composer_env = "composer_#{env}".to_sym
    task composer_env do
      sh "composer install -d #{BUILD_DIR} --no-dev --optimize-autoloader --no-interaction"
    end
    task build_env => [composer_env]
  end

  desc "Upload #{env} environment to the configured host."
  task upload_env => build_env do
    # Create target folder:
    sh "ssh #{release_host} '([ -d #{release_path} ] || mkdir -p #{release_path})'"
    # If settings folder is read-only, make it writable
    sh "ssh #{release_host} 'if [ -d #{release_path}/sites/default ] && [ ! -w #{release_path}/sites/default ]; then chmod g+w #{release_path}/sites/default; fi'"
    # Verify exclude paths (and potentially add more or use excludes-file)
    # TODO: add check for exclude_deploy, and use: --exclude-from=exclude_deploy instead
    # --exclude config.rb --exclude sass/ --exclude .sass-cache/
    rsync_options = "-rz --stats --exclude sites/default/files/ --delete"
    rsync_source = "#{BUILDDIR}/#{env}/#{DRUPAL}/"
    rsync_target = "#{release_host}:#{release_path}"
    # Sync files...
    sh "rsync #{rsync_options} #{rsync_source} #{rsync_target}"
    # Vendor is inside web :-/
    # task :deploy_vendor => [:build] do
    #   sh "rsync -rz --delete --links #{BUILD_DIR}/vendor/ #{DEPLOY_TARGET}/vendor/"
    # end
    # Make settings folder read-only again:
    sh "ssh #{release_host} '([ -w #{release_path}/sites/default ] && chmod g-w #{release_path}/sites/default)'"
  end

  db_backup_task = "db_backup_#{env}".to_sym
  task db_backup_task do
    file = "#{release_backups}/backup-#{env}-#{DateTime.now}.sql.gz"
    sh "ssh #{release_host} '#{drush_path} -r #{release_path} sql-dump --structure-tables-key=common --gzip > #{file}'"
  end

  db_drop_tables_task = "db_drop_tables_#{env}".to_sym
  task db_drop_tables_task => db_backup_task do
    sh "ssh #{release_host} #{drush_path} -y -r #{release_path} sql-drop"
  end

  drush_test_task = "drush_test_#{env}".to_sym
  desc "Display the remote drush status"
  task drush_test_task do
    sh "ssh #{release_host} #{drush_path} -r #{release_path} status"
  end

  desc "Deploy the #{env} environment to the configured host."
  task deploy_env => [db_backup_task, upload_env] do
    files_path = "#{release_path}/sites/default/files"
    if TYPE == "Drupal7" then
      commands = [
      "([ -d #{files_path} ] || mkdir #{files_path})",
      "drush -y -r #{release_path} updatedb",
      "drush -y -r #{release_path} cc all",
      ].join(" && ")
    elsif TYPE == "Drupal8" then
      commands = [
        "([ -d #{files_path} ] || mkdir #{files_path})",
        "#{drush_path} -y -r #{release_path} cr",
        "#{drush_path} -y -r #{release_path} cim",
        "#{drush_path} -y -r #{release_path} updatedb",
        "#{drush_path} -y -r #{release_path} cr",
      ].join(" && ")
    end
    sh "ssh #{release_host} '#{commands}'"
  end

  # unless defined? CONFIG_REPO
  #   task :deploy_config => [:build] do
  #   # c=use checksum (avoid missing updates)
  #     sh "rsync -rzc --delete #{BUILD_DIR}/config/ #{DEPLOY_TARGET}/config/"
  #   end
  # end

  file_sync_task = "file_sync_#{env}_to_local".to_sym
  desc "Sync files from #{env} to local environment."
  task file_sync_task do
    sh "rsync -rz --stats --exclude styles --exclude css --exclude js --exclude php --delete \
      #{release_host}:#{release_path}/sites/default/files/ \
      #{DRUPAL}/sites/default/files/"
  end

  db_sync_task = "db_sync_#{env}_to_local".to_sym
  desc "Sync database from #{env} to local environment."
  task db_sync_task do
    drupal_root = "#{Dir.getwd()}/#{DRUPAL}"
    sh "drush -y -r #{drupal_root} sql-dump > /tmp/paranoia.sql"
    sh "drush -y -r #{drupal_root} sql-drop"
    sh "ssh -C #{release_host} #{drush_path} -r #{release_path} \
      sql-dump --structure-tables-key=common | drush -r #{drupal_root} sql-cli"
  end

  ENVIRONMENTS.keys.each do |e|
    unless e == env then
      from_host = ENVIRONMENTS[e]["host"]
      from_path = ENVIRONMENTS[e]["path"]
      from_drush = ENVIRONMENTS[e]["drush"]
      from_drush = from_drush ? from_drush : "drush"

      file_sync_task = "file_sync_#{e}_to_#{env}".to_sym
      desc "Sync files from #{e} to #{env} environment."
      task file_sync_task do
        sh "ssh -A #{from_host} rsync -rz --stats --exclude styles \
          --exclude css --exclude js #{from_path}/sites/default/files/ \
          --delete " + (from_host == release_host unless (NO_HOST_SHORTCUTS).defined? ? "" : "#{release_host}:") + "#{release_path}/sites/default/files/"
      end

      db_sync_task = "db_sync_#{e}_to_#{env}".to_sym
      desc "Sync database from #{e} to #{env} environment."
      task db_sync_task => db_drop_tables_task do
        sh "ssh -C #{from_host} #{from_drush} -r #{from_path} \
          sql-dump --structure-tables-key=common | \
          ssh -C #{release_host} #{drush_path} -r #{release_path} sql-cli"
      end
    end
  end

  desc "Build all environments."
  task :default => build_env
end

TAGNAMES.each do |tagname|
  desc "Tag a commit with #{tagname}."
  task "tag_#{tagname}".to_sym do
    sh "git fetch --tags"
    num = `git tag`.scan(Regexp.new(tagname + "-")).size + 1
    sh "git tag -am '#{tagname.upcase} Release #{num}' #{tagname}-#{num}"
    sh "git tag -afm 'Current #{tagname.upcase} Release' #{tagname}"
    sh "git push origin :refs/tags/#{tagname}"
    sh "git push origin --tags"
  end
end

desc "Detect coding standard violations in profile and custom modules."
task :sniff do
  files = ["web/profiles/#{PROFILE}", 'web/sites/all/modules/custom'].join(' ')
  extensions = ['module', 'profile', 'install', 'inc', 'php'].join(',')
  sh "phpcs --extensions=#{extensions} #{files}"
end

namespace :tests do
  desc "Run integration tests."
  task "integration" do
    # -c tests/phpunit-integration.xml <- configuration
    # https://phpunit.readthedocs.io/en/9.0/textui.html
    sh "phpunit --process-isolation --bootstrap=tests/bootstrap.php tests/integration"
    # Alt:
    # sh "phpunit --bootstrap=tests/bootstrap.php -c tests/phpunit-integration.xml"
  end

  desc "Run Behat tests."
  task :behat => [:vendor] do
    sh "./vendor/bin/behat --tags '~@javascript' --format pretty --out std --format junit --out reports"
  end

  desc "Run performance tests."
  task :performance => [:vendor] do
    sh "./vendor/bin/phpunit --bootstrap ./vendor/autoload.php --log-junit reports/performance.xml tests/"
  end
end

if TYPE == "Drupal8" then
  desc "Apply updates locally."
  task :update_local => [:vendor] do
    sh "./#{DRUSH_BIN} -y updb"
    sh "./#{DRUSH_BIN} cr"
    sh "./#{DRUSH_BIN} -y cim"
    sh "./#{DRUSH_BIN} cr"
  end
  task :vendor do
    sh "composer install"
  end
end

if defined? PROFILE then
  desc "Delete and re-install a site from its installation profile."
  task "site_install" => "compass_compile" do
    sh "#{drush_path} -y site-install #{PROFILE}"
    # sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web -y si #{PROFILE} install_configure_form.update_status_module='[0, 0]'"
    chmod_R "u+w", DEFAULT_DIR
  end
end

# desc "Compile Sass to CSS in the local environment."
# task "compass_compile" do
#   Dir.glob("#{DRUPAL}/sites/all/themes/**/config.rb") do |project|
#     sh "compass compile #{File.dirname(project)}"
#   end
# end

# desc "Compile CSS."
# task :css do
#   sh "sass -t expanded --precision 10 web/sites/all/themes/eliot/sass/stylesheet.scss web/sites/all/themes/eliot/dist/css/stylesheet.css"
#   sh "sass --precision 10 #{BUILD_DIR}/web/sites/all/themes/eliot/sass/stylesheet.scss #{BUILD_DIR}/web/sites/all/themes/eliot/dist/css/stylesheet.css"
#   sh "sass -E utf-8 web/themes/popeye/sass/styles.scss web/themes/popeye/css/styles.css"
#   sh "sass -E utf-8 web/themes/popeye/sass/ckeditor.scss web/themes/popeye/css/ckeditor.css"
# end

# desc "Warm the site's cache by crawling every page."
# task :warm do
#   if not WARM_URL.empty? then
#       sh "wget -r -nd --delete-after --http-user=#{WARM_USER} --http-password=#{WARM_PASSWORD} #{WARM_URL} || echo wget says: $?"
#   else
#       puts "You must supply a target url or a configured environment.\n" \
#           "For example, to target the live instance:\n" \
#           "ENVIRONMENT=live rake warm\n" \
#           "Or, to target any URL, do not specify ENVIRONMENT, and instead use WARM_URL (and optionally WARM_USER and WARM_PASSWORD):\n" \
#           "WARM_URL=https://duckduckgo.com rake warm"

#   end
# end

# namespace :cf do
#   desc "Display zone information from CloudFlare"
#   task :list_zones do
#     sh "KEY=`cat .cf_api_key.txt` && \
#       curl -X GET \"https://api.cloudflare.com/client/v4/zones?name=massdesigngroup.org&status=active&page=1&per_page=20&order=status&direction=desc&match=all\" \
#       -H \"X-Auth-Email: #{CF_USER_ID}\" \
#       -H \"X-Auth-Key: $KEY\" \
#       -H \"Content-Type: multipart/form-data\""
#   end

#   desc "Purge caches"
#   task :purge do
#     sh "KEY=`cat .cf_api_key.txt` && \
#       curl -X DELETE \"https://api.cloudflare.com/client/v4/zones/#{CF_ZONE_ID}/purge_cache\" \
#       -H \"X-Auth-Email: #{CF_USER_ID}\" \
#       -H \"X-Auth-Key: $KEY\" \
#       -H \"Content-Type: application/json\" \
#       --data '{\"purge_everything\":true}'"
#   end

#   desc "Perform a recursive download of site."
#   task :warm do
#    sh "wget -r -nd --delete-after http://massdesigngroup.org"
#   end
# end